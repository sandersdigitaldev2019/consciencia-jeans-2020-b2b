var tables;

$(document).ready(function(){
  trace('product.js | 0.1');
  if($('body').hasClass('produto')) {
    //MUDANÇA DO TAMANHO DAS IMAGENS 
    $('.apresentacao .thumbs img').each(function(){
      $(this).attr({
        'class' : 'image-zoom',
        'data-zoom-image' : $(this).attr('src').replace('70-70', '1000-1000'),
        'src' : $(this).attr('src').replace('70-70', '1000-1000')
      });
    });

    //SLIDE GALLERY
    $('.apresentacao .thumbs').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1
    });

    var amountGrid = $('.j_product-price').find('.specification .value-field.QUANTIDADE-PACK-VTEX').text(); // Quantidade de produtos na grade
    var amountPack = 1; //Quantidade de PACK de produtos
    var inputAmount = $('.j_product-price').find('input[name="product-single-price-amount"]'); // Quantidade inicial
    var productId = skuJson.skus[0].sku; //controller.getProduct().productId; //Id do produto
    var sellerId = skuJson_0.skus[0].sellerId; //controller.getProduct().skus[0].sellerId; // Id do Seller
    var gridProduct =  $('.j_product-price').find('.specification .value-field.GRUPOS-VTEX').text(); //Pega a grade do produto

    var Product = new (function(){

      this.init = function() {
        //Adiciona os preços
        var priceTotal = Product.calculatePrice(amountGrid, amountPack, skuJson.skus[0].bestPrice);
        Product.changePricesFull(skuJson.skus[0].listPriceFormated, skuJson.skus[0].bestPriceFormated, priceTotal);
    
        //Seta as quantidade iniciais
        Product.changeAmountPackage(amountGrid, amountPack);

        //Seta a grade
        Product.showGrid(); 
      }

      //Método que aplica os novos valores
      this.changePricesFull = function(best, unitary, total) {
        $('.j_product-single-values-best').text(best)
        $('.j_product-single-values-unitary').text(unitary)
        $('.j_product-single-values-total').text(total)
      }

      //Atualiza preço total
      this.changePriceTotal = function(total){
        $('.j_product-single-values-total').text(total)
      }

      //Atualiza quantidade de pack
      this.updateAmountPack = function(amountPack) {
        
        $('.j_product-amount-package').text(amountPack);
      }

      //Método que muda as quantidades
      this.changeAmountPackage = function(amountGrid, amountPack){
        $('.j_product-amount-grid').text(amountGrid);
        $('.j_product-amount-package').text(amountPack);
      }

      //Método que calcula o preço total
      this.calculatePrice = function(amountGrid , amountPack, unitary) {
        return 'R$ '+ qFormatReal(((amountGrid * amountPack) * unitary));
      }

      //Método que exibe detalhes da grade
      this.showGrid = function() {
        // Informativo baseado nas especificações dos produtos
        var grid;
        var createGrid = true;
        switch (gridProduct){
          case "2P-2M-1G-1GG" :
          grid = {
            'th' : ['pp', 'p', 'm', 'g', 'gg', 'xg', 'xgg'],
            'td' : ['-', '2', '2', '1', '1', '-', '-']
          } 
          break;
          case "1P-2M-2G-1GG" :
          grid = {
            'th' : ['pp', 'p', 'm', 'g', 'gg', 'xg', 'xgg'],
            'td' : ['-', '1', '2', '2', '1', '-', '-']
          } 
          break;
          case "236-238-240-142-144-146" :
          grid = {
            'th' : ['36', '38', '40', '42', '44', '46', '48'],
            'td' : ['2', '2', '2', '1', '1', '1', '-']
          } 
          break;
          
          case "236-238-240-142-144" :
          grid = {
            'th' : ['36', '38', '40', '42', '44', '46', '48'],
            'td' : ['2', '2', '2', '1', '1', '-', '-']
          } 
          break;
          
          case "136-138-240-242-244-1-46-148" :
          grid = {
            'th' : ['36', '38', '40', '42', '44', '46', '48'],
            'td' : ['1', '1', '2', '2', '2', '1', '-']
          } 
          break;

          case "136-238-240-242-144" :
          grid = {
            'th' : ['36', '38', '40', '42', '44', '46', '48'],
            'td' : ['1', '2', '2', '1', '1', '-', '-']
          } 
          break;
          default:
          createGrid = false;
          trace('Especificação de grade inválida: invalida');
          addMicroInformacao('.j_product-grid-insert', 'Houve um erro ao gerar a grade <i class="icon-notext icon-smiley-frown"></i>.', 'trigger-error-out', true);
          break;
        }
        // Grade do Produto
        if(createGrid) {
          Product._createTable(grid);
        }
      }

      //Método responsável por criar a tabela
      this._createTable = function(data) {
        var table = $('<table>').attr({'class':'table','border': '1', 'cellspacing':'0', 'rules':'none'});
        var thead = $('<thead>').addClass("table-head");
        var tbody = $('<tbody>').addClass("table-body");
        var th, td;

        data.th.forEach(function(text){
          th += '<th class="table-tr-item">'+text+'</th>';
        });

        data.td.forEach(function(text){
          td += '<td class="table-tr-item">'+text+'</td>';
        });

        thead.append($('<tr>').addClass('table-tr').append(th))
        tbody.append($('<tr>').addClass('table-tr').append(td))

        table.append(thead);
        table.append(tbody);      

        $('.j_product-grid-insert').append(table);

        Product._inactiveCellsTable();
      }

      //Método que inativa celulas
      this._inactiveCellsTable = function() {
        $('.table td.table-tr-item').each(function(text, v){
          value =  $(v).text();
          if(value == '-') {
          $('.box_grade table td.table-tr-item:eq(' + text + '), .box_grade table th.table-tr-item:eq(' + text + ')').addClass('inactive')
          }
        })
      }      

      //Adiciona produto no carrinho
      this.addToCart = function(productAmount) {
        if(productAmount != 0 && productAmount) {
          var item = '/checkout/cart/add?sku=' + productId + '&qty=' + productAmount + '&seller=1&amp;redirect=false&sc=1';
          
          $.get(item, function(data){
            vtexjs.checkout.getOrderForm()
            .done( function( orderForm ) {
              addMicroInformacao('.j_product-purchase', 'Seus produtos foram enviados para o carrinho <i class="icon-notext icon-smile"></i>.', 'trigger-success-out');
              // Indicar ao carrinho que ele pode aceitar a atualização que receberá00
              qLib.miniCart.waitingByUpdateToggle();
              // Informa novo Order e dispara o evento de atualizações
              controller.cartSet(orderForm);
              // Abre o carrinho
              qLib.miniCart.open();
            });
          }).fail( function( data ) {
            addMicroInformacao('.j_product-purchase', 'Erro ao adicionar o produto, tente novamente mais tarde <i class="icon-notext icon-smiley-frown"></i>.', 'trigger-error-out');
          });
        } else {
          addMicroInformacao('.j_product-purchase', 'A quantidade precisa ser maior que zero <i class="icon-notext icon-smiley-meh"></i>.', 'trigger-alert-out');
        }
      }
      
    });

    //Inicializa o setup da página
    Product.init();

    //Muda a quantidade da compra
    $('.j_product-price').on('click', '.product-single-price-form button', function(e){
      e.preventDefault()
      var amount = parseInt($('.j_product-price').find('input[name="product-single-price-amount"]').val());
      inputAmount = $('.j_product-price').find('input[name="product-single-price-amount"]');
      var action = $(this).data('action');

      if(action == 'sum') {
        amount = amount+1;
        inputAmount.val(amount);
        Product.updateAmountPack(amount);
        Product.changePriceTotal(Product.calculatePrice(amountGrid, amount, skuJson.skus[0].bestPrice));

      } else if(action == 'subtraction') {
        amount = amount-1;
        amount = (amount == 0) ? '1' : amount;
        
        inputAmount.val(amount);
        Product.updateAmountPack(amount);
        Product.changePriceTotal(Product.calculatePrice(amountGrid, amount, skuJson.skus[0].bestPrice));
      } 
    });

    //Mida a quantidade da compra pelo input
    $('.j_product-price').on('input', 'input', function() {
      var input = $(this);
      var amount = input.val().replace(/[^0-9]/g, '');
      if(amount) {
        if(amount == 0) {
          amount = 1;
          input.val(amount);
          addMicroInformacao('.j_product-trigger', 'A quantidade minima é 1 (um) <i class="icon-notext icon-smiley-meh"></i>.', 'trigger-alert-out');
        }
        Product.updateAmountPack(amount);
        Product.changePriceTotal(Product.calculatePrice(amountGrid, amount, skuJson.skus[0].bestPrice));
      } else {
        input.val('').focus();
        addMicroInformacao('.j_product-trigger', 'Você precisa informar apenas números e precisa ser no mínimo 1 <i class="icon-notext icon-smiley-meh"></i>.', 'trigger-alert-out');
      }
    });

    //Chama login
    vtexid.setScope('a08e4cdf-0b16-4f1c-9973-8cabdd7db1bb');
    vtexid.setScopeName('conscienciajeans');
    $('body').on('click', '.j_product-login', function () {
      vtexid.start({returnUrl: window.location.pathname, userEmail: '', locale: 'pt-BR', forceReload: false});
    });

    //Adiciona produto no carrinho
    $('.j_product-purchase').on('click', '.j_product-purchase-buy', function(e){
      e.preventDefault();
      Product.addToCart(parseInt(inputAmount.val()));
    });
  }
});