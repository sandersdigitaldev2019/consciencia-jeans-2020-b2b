$(function () {
  $('header #cart .finish').hide()
  setTimeout(function() {
    if($('.orderBy_new label[index="4"]').length > 0){
        $('.orderBy_new label[index="4"]').click()
    }
  }, 500);
  $.ajax({
    url: "/api/vtexid/pub/authenticated/user",
    type: "GET",
  }).done(function (response) {
    if (response != null) {
      $.ajax({
        type: "GET",
        dataType: "json",
        url:
          "/api/dataentities/CL/search?_fields=bloquearPedido%2Cblacklist&email=" +
          response.user,
      }).done(function (res) {
        if (res.length == 1) {
          console.log(res[0]);
          if (res[0].bloquearPedido == true) {
            $("article#cart .finish").remove();
            $("#continue_buy .footer ul li:nth-child(2)").remove();
            $(".column_4 .ul .cart").on('click', function(e){
              e.preventDefault();
            })
          }
          if (res[0].bloquearPedido == "t") {
              $("article#cart .finish").remove();
              $("#continue_buy .footer ul li:nth-child(2)").remove();
              $(".column_4 .ul .cart").on('click', function(e){
                  e.preventDefault();
              })
          }
          if(res[0].bloquearPedido != true && res[0].bloquearPedido != "T"){
            $('header #cart .finish').show()
          }
        }else{
          $('header #cart .finish').show()
        }
      });
    }else{
      $('header #cart .finish').show()
    }
  });
});