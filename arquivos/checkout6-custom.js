//IMPORTANDO LIB
$('body').prepend('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>');
$('body').append('<div class="popupCNPJ" style="display: none"><div class="overlay_popupcnpj" style="position: fixed;top: 0;bottom: 0;left: 0;right: 0;background: rgb(0,0,0,0.6);z-index: 2;"></div><div class="popup" style="display: block;position: fixed;top: 50%;z-index: 2;background: #fff;border: 1px solid #333;padding: 20px;font-size: 20px;max-width: 270px;text-align: center;line-height: 30px;left: 50%;transform: translate(-50%, -50%);"><div class="close_popupcnpj" style="cursor: pointer;position: absolute;right: 5px;top: -25px;color: #fff;opacity: 1;text-shadow: none;">X</div><p>Verificamos que o seu CNPJ não está ativo, por favor entre em contato com o SAC.</p></div></div>');
$(function () {
    $("#cart-to-orderform, #payment-data-submit").hide();
    $.ajax({
      url: "/api/vtexid/pub/authenticated/user",
      type: "GET",
    }).done(function (response) {
      if (response != null) {
        $.ajax({
          type: "GET",
          dataType: "json",
          url:
            "/api/dataentities/CL/search?_fields=bloquearPedido%2Cblacklist%2CcnpjInapto&email=" +
            response.user,
        }).done(function (res) {
          if (res.length == 1) {
            console.log(res[0]);
            if (res[0].bloquearPedido == true) {
              $("#cart-to-orderform, #payment-data-submit").remove();
            }
            if (res[0].bloquearPedido == "T") {
                $("#continue_buy .footer ul li:nth-child(2)").remove();
            }
            if (res[0].cnpjInapto == true) {
                $(".popupCNPJ").show();
                $("body").on("click", '.close_popupcnpj', function(){
                    $(".popupCNPJ").hide();
                })
                $("body").on("click", '.overlay_popupcnpj', function(){
                    $(".popupCNPJ").hide();
                })
            }
          }else{
            $("#cart-to-orderform, #payment-data-submit").show();
          }
        });
      }else{
        $("#cart-to-orderform, #payment-data-submit").show();
      }
    });
});

var barra = function () {
    $(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
        var init = 12;
        $.each(orderForm.items, function (index, item) {
            init = init -= item.quantity;
        });

        if (orderForm.items.length === 0) {
            $('#message_frete').hide();
            $('#barra_frete .column-2 .barra span').css('width', '100%');
            $('#barra_frete .column-1 .message_default strong, #message_frete .message_default strong').text(12);
        } else {
            $('#message_frete').show();

            //BARRA_FRETE
            if (init <= 0) {
                $('#barra_frete, #message_frete').addClass('success');
                $('#cart-to-orderform, #payment-data-submit').removeClass('disabled');
            } else {
                $('#barra_frete, #message_frete').removeClass('success');
                $('#cart-to-orderform, #payment-data-submit').addClass('disabled');
                $('#barra_frete .column-2 .barra span').css('width', init + '%');
                $('#barra_frete .column-1 .message_default strong, #message_frete .message_default strong').text(init);
            }
        }
    });
}
barra();

$(document).ajaxStop(function () {
    barra();
});

//ABRE BOX INFO PJ
$(document).on('click', '#edit-profile-data', function () {
    setTimeout(function () {
        $(document).find('#is-corporate-client').trigger('click');
    }, 500);
});

//REMOVE OPÇÃO 'NÃO PJ'
$('#not-corporate-client').remove();

$(window).on('load', function () {
    var preencher = function () {
        function validate() {
            let text = '';
            switch (text) {
                case $('#client-company-name').val():
                    //Razão Social
                    $('button[id="payment-data-submit"]').addClass('disabled');
                    swal('Oops', 'Preencha o campo: Razão Social', 'warning');
                    break;
                case $('#client-company-nickname').val():
                    //Nome Fantasia
                    $('button[id="payment-data-submit"]').addClass('disabled');
                    swal('Oops', 'Preencha o campo: Nome Fantasia', 'warning');
                    break;
                case $('#client-company-ie').val():
                    //Inscrição Estadual
                    $('button[id="payment-data-submit"]').addClass('disabled');
                    swal('Oops', 'Preencha o campo: Inscrição Estadual', 'warning');
                    break;
                case $('#client-company-document').val():
                    //CNPJ
                    $('button[id="payment-data-submit"]').addClass('disabled');
                    swal('Oops', 'Preencha o campo: CNPJ', 'warning');
                    break;
                default:
                    //LIBERADO PARA COMPRA
                    $('button[id="payment-data-submit"]').removeClass('disabled');
            }
        };

        $('#go-to-shipping, #payment-data-submit').on('mouseenter', function () {
            validate();
        });
    };
    preencher();

    $('h2.empty-cart-title').text('');
    $('h2.empty-cart-title').text('Carrinho Vazio');

    $('.empty-cart-message p').text('');
    $('.empty-cart-message p').text('Parece que o seu carrinho está vazio. navegue pelas categorias do site ou faça uma busca.');

    if ($('.empty-cart-content').attr('style') === 'display: block;') {
        $('#cart-title').addClass('hiden');
        $('body.body-cart .cart-template-holder').addClass('active');
    }
});