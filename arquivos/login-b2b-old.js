var header = {
	'Accept': 'application/json',
	'REST-range': 'resources=0-100',
	'Content-Type': 'application/json; charset=utf-8'
};

var insertMasterData = function (ENT, loja, dados, fn) {
	$.ajax({
		url: window.location.origin + '/' + loja + '/dataentities/' + ENT + '/documents/',
		type: 'PATCH',
		data: dados,
		headers: header,
		success: function (res) {
			fn(res);
		},
		error: function (res) {}
	});
};

var selectMasterData = function (ENT, loja, params, fn) {
	$.ajax({
		url: window.location.origin + '/' + loja + '/dataentities/' + ENT + '/search?' + params,
		type: 'GET',
		headers: header,
		success: function (res) {
			fn(res);
		},
		error: function (res) {}
	});
};

//MASCARAS
$("input[name='cnpj']").mask("99.999.999/9999-99");
$('#form_new_user input[name="cpf"]').mask('000.000.000-00', {
	reverse: true
});
$('#form_new_user input[name="celular"]').mask('(00) 00000-0000');
$('#form_new_user input[name="phone"]').mask('(00) 0000-0000');
$('#form_new_user input[name="cep"]').mask('00000-000');

var api = (function () {
	var login = {
		login: function () {
			$('#login').submit(function (event) {
				event.preventDefault();

				$.ajax({
					type: 'GET',
					headers: header,
					url: '/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=atacadoconscienciajeans'
				}).
				done(function (response) {
					var email_login = $('#login input[type="email"]').val();
					var senha_login = $('#login input[name="senha"]').val();

					$.ajax({
						type: 'POST',
						url: '/api/vtexid/pub/authentication/classic/validate?authenticationToken=' + response.authenticationToken + '&login=' + email_login + '&password=' + senha_login
					}).
					done(function (res) {
						console.log(res);
						if (res.authStatus == 'WrongCredentials') {
							swal("Oops", "Senha inválida!", "error");
						} else if (res.authStatus = 'Success') {
							//RETONAR A PÃ�GINA DO PRODUTO
							if (localStorage.getItem('last_product')) {
								window.location = localStorage.getItem('last_product');
								localStorage.removeItem('last_product');
							} else {
								window.location = location.origin;
							}
						}
					});
				});
			});
		},

		view_password: function () {
			$('#login .view').on('click', function () {
				let input = $('#login input[name="senha"]');

				if (input.attr('type') === 'password') {
					input.prop('type', 'text');
				} else {
					input.prop('type', 'password');	
				}
			});
		},

		forgot_password: function () {
			$('.btn_forgot').on('click', function (event) {
				event.preventDefault();

				$('#alerta .step_1').fadeOut(300, function () {
					setTimeout(function () {
						$('#alerta .step_2').fadeIn(300);
					}, 300);
				});
			});
		},

		forgot_send_key: function () {
			//GERA TOKEN
			$('#forgot_password').submit(function (event) {
				event.preventDefault();
				$.ajax({
					type: 'GET',
					headers: header,
					url: '/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=atacadoconscienciajeans'
				}).
				done(function (start) {
					//ENVIA TOKEN PARA EMAIL
					var email = $('#forgot_password input[name="forgot_email"]').val();

					$.ajax({
						type: 'POST',
						url: "/api/vtexid/pub/authentication/accesskey/send?authenticationToken=" + start.authenticationToken + "&email=" + email
					}).done(function (response) {
						swal("Chave de acesso!", "A chave de acesso foi enviada para o seu email.", "success");

						$('#change_password input[name="confirm_email"]').attr('value', email);

						$('#alerta .step_2').fadeOut(300, function () {
							setTimeout(function () {
								$('#alerta .step_3').fadeIn(300);
							}, 300);
						});
					});
				});
				//FIM - ENVIA TOKEN PARA EMAIL
			});
		},

		change_password: function () {
			//NOVA SENHA
			$('#change_password').submit(function (event) {
				event.preventDefault();

				var senha_1 = $('#change_password input[name="senha_1"]').val();
				var senha_2 = $('#change_password input[name="senha_2"]').val();

				if (senha_1.length >= 8 && senha_2.length >= 8) {

					if (senha_1 === senha_2) {
						var email_login = $('#change_password input[name="confirm_email"]').val();
						var senha_login = $('#change_password input[name="senha_1"]').val();
						var autentica_login = $('#change_password input[name="confirm_chave"]').val();

						var settings = {
							"async": true,
							"crossDomain": true,
							"url": "/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=atacadoconscienciajeans",
							"method": "GET"
						}

						$.ajax(settings).done(function (response) {
							var entrada = {
								"async": true,
								"crossDomain": true,
								"url": "/api/vtexid/pub/authentication/classic/setpassword?authenticationToken=" + response.authenticationToken + "&newPassword=" + senha_login + "&login=" + email_login + "&accessKey=" + autentica_login,
								"type": "POST"
							}
							$.ajax(entrada).done(function (response) {
								if (response.authStatus == 'WrongCredentials') {
									swal("Oops", "Chave inválida!", "error");
								} else {
									console.log(response);
									swal("Nova senha!", "Sua senha foi alterada com sucesso.", "success");
									window.location = window.origin;
								}
							});
						});
					} else {
						swal("Oops", "Senha inválida!", "error");
					}
				}
			});
		}
	}

	login.login();
	login.view_password();
	login.forgot_password();
	login.forgot_send_key();
	login.change_password();

	var start = {
		valida_cnpj: function (cnpj) {
			cnpj = cnpj.replace(/[^\d]+/g, '');

			if (cnpj == '') return false;

			if (cnpj.length != 14)
				return false;

			// Elimina CNPJs invalidos conhecidos
			if (cnpj == "00000000000000" ||
				cnpj == "11111111111111" ||
				cnpj == "22222222222222" ||
				cnpj == "33333333333333" ||
				cnpj == "44444444444444" ||
				cnpj == "55555555555555" ||
				cnpj == "66666666666666" ||
				cnpj == "77777777777777" ||
				cnpj == "88888888888888" ||
				cnpj == "99999999999999")
				return false;

			// Valida DVs
			tamanho = cnpj.length - 2
			numeros = cnpj.substring(0, tamanho);
			digitos = cnpj.substring(tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0))
				return false;

			tamanho = tamanho + 1;
			numeros = cnpj.substring(0, tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1))
				return false;

			return true;
		},

		consulta_cnpj: function () {
			$('#consulta_cnpj').submit(function (event) {
				event.preventDefault();
				//CNPJ
				input_cnpj = $('#consulta_cnpj input[name="cnpj"]').val();
				
				// //LOADING
				$('#consulta_cnpj input[type="submit"]').addClass('loading');

				if (start.valida_cnpj(input_cnpj)) {
					$.ajax({
						type: 'GET',
						headers: header,
						url: "/atacadoconscienciajeans/dataentities/CL/search?_fields=corporateDocument&corporateDocument=" + input_cnpj
					}).
					done(function (response) {
						if (response.length === 0) {
							//CNPJ NÃƒO ENCONTRADO NO MASTER DATA - CONSULTA API
							$.ajax({
								method: "POST",
								type: "POST",
								url: "http://rafaeljk.com.br/conscienciajeans/consulta-cnpj.php",
								// url: "https://www.rafaelmalfi.com.br/cnpj/soawebservice.php",
								data: {
									"document": input_cnpj
								},
								success: function (response) {
									var res = JSON.parse(response);
									if(res.SituacaoRFB != "INAPTA"){
										console.log(response);
										var isMatch = [];

										if (res.Mensagem != 'Transacao realizada com sucesso!') {
											swal("Oops", res.Mensagem, "error");
										} else {
											//HIDE
											$('#consulta_cnpj').fadeOut(300);

											//INICIO DO CADASTRO
											$('#form_new_user').removeClass('disabled');

											//CPF
											var string_document = res.Documento;
											var documento = parseInt(string_document);

											//PREECHE O FORMULÃ�RIO
											//INFO EMPRESA
											$('#form_new_user input[name="cnpj"]').val(input_cnpj);
											$('#form_new_user input[name="razao-social"]').val(res.RazaoSocial);
											$('#form_new_user input[name="nome-fantasia"]').val(res.NomeFantasia);
											$('#form_new_user input[name="data-fundacao"]').val(res.DataFundacao);
											$('#form_new_user input[name="matriz-filial"]').val(res.MatrizFilial);
											$('#form_new_user input[name="capital"]').val(res.Capital);
											$('#form_new_user input[name="cod-atividade-economica"]').val(res.CodigoAtividadeEconomica);
											$('#form_new_user input[name="cod-atividade-economica-descricao"]').val(res.CodigoAtividadeEconomicaDescricao);
											$('#form_new_user input[name="cod-natureza-juridica"]').val(res.CodigoNaturezaJuridica);
											$('#form_new_user input[name="cod-natureza-juridica-descricao"]').val(res.CodigoNaturezaJuridicaDescricao);
											$('#form_new_user input[name="situacao-rfb"]').val(res.SituacaoRFB);
											$('#form_new_user input[name="data-situacao-rfb"]').val(res.DataSituacaoRFB);
											$('#form_new_user input[name="data-consulta-rfb"]').val(res.DataConsultaRFB);
											$('#form_new_user input[name="ie"]').val(res.InscricaoEstadual);

											if (res.CNAES.length != 0) {
												$('#form_new_user input[name="cnae"]').val(res.CNAES[0].CNAE);
												$('#form_new_user input[name="cnae-descricao"]').val(res.CNAES[0].CNAEDescricao);
											}
											//FIM - INFO EMPRESA

											//INFO PESSOAL
											$('#form_new_user input[name="phone"]').val(res.Telefone);
											//FIM - INFO PESSOAL

											//INFO ENDEREÃ‡O
											$('#form_new_user input[name="cep"]').val(res.Enderecos[0].CEP);
											$('#form_new_user input[name="logradouro"]').val(res.Enderecos[0].Logradouro);
											$('#form_new_user input[name="numero"]').val(res.Enderecos[0].Numero);
											$('#form_new_user input[name="complemento"]').val(res.Enderecos[0].Complemento);
											$('#form_new_user input[name="bairro"]').val(res.Enderecos[0].Bairro);
											$('#form_new_user input[name="cidade"]').val(res.Enderecos[0].Cidade);
											$('#form_new_user input[name="estado"]').val(res.Enderecos[0].Estado);
											$('#form_new_user input[name="data-de-atualizacao"]').val(res.Enderecos[0].DataAtualizacao);
											$('#form_new_user input[name="ibge"]').val(res.Enderecos[0].CodigoIBGE);
											//FIM - INFO ENDEREÃ‡O
										}

										$('#consulta_cnpj input[type="submit"]').removeClass('loading');
									}else{
										swal("Oops", "CNPJ COM SITUAÇÃO CADASTRAL INAPTA", "error");
										$('#consulta_cnpj input[type="submit"]').removeClass('loading');
									}
								},
								error: function (res) {
									swal("Oops", "Algo deu errado!", "error");

									$('#consulta_cnpj input[type="submit"]').removeClass('loading');
								}
							});
						} else {
							//CNPJ ENCONTRADO NO MASTER DATA - JÃ� TEM CADASTRO
							console.log('CNPJ ENCONTRADO NO MASTER DATA - JÁ TEM CADASTRO');
							swal("Oops", "CNPJ já cadastrado!", "warning");

							//REMOVE LOADING
							$('#consulta_cnpj input[type="submit"]').removeClass('loading');
						}
					});
				} else {
					swal("Oops", "CNPJ Inválido!", "error");

					//REMOVE LOADING
					$('#consulta_cnpj input[type="submit"]').removeClass('loading');
				}
			});
		}
	}

	start.consulta_cnpj();

	var cadastro = {
		send_token: function () {
			//GERA TOKEN
			$('#send_token').submit(function (event) {
				event.preventDefault();
				$.ajax({
					url: '/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=atacadoconscienciajeans',
					type: 'GET',
					headers: header
				}).done(function (start) {
					//ENVIA TOKEN PARA EMAIL
					var email = $('#send_token input[type="email"]').val();
					var token = start.authenticationToken;

					$.ajax({
						url: "/api/vtexid/pub/authentication/accesskey/send?authenticationToken=" + token + "&email=" + email,
						type: 'POST'
					}).done(function (response) {
						swal("Chave de acesso!", "A chave de acesso foi enviada para o seu email.", "success");

						//PREENCHE O CAMPO EMAIL DO FORM 2
						var valEmail = $('#send_token input[type="email"]').val();
						$('#create_account input[name="confirm_email"], #form_new_user input[name="email"]').attr('value', valEmail);
						$('#form_new_user input[name="email"]').attr('value', valEmail);

						$('#alerta .step_1').fadeOut(300, function () {
							setTimeout(function () {
								$('#alerta .step_2').fadeIn(300);
							}, 300);
						});
					});
				});
				//FIM - ENVIA TOKEN PARA EMAIL
			});
		},

		validate_password: function () {
			$('#create_account input[name="senha_1"], #change_password input[name="senha_1"]').on('input', function (a, e) {
				let element = $(this).val();
				var upperCase = new RegExp('[A-Z]');
				var lowerCase = new RegExp('[a-z]');
				var numbers = new RegExp('[0-9]');

				if (element != '') {
					//min length: 8
					if (element.length >= 8) {
						$('.validate_password[data-name="caracter"]').addClass('active');
					} else {
						$('.validate_password[data-name="caracter"]').removeClass('active');
					}

					//is number
					if ($(this).val().match(numbers) != null) {
						$('.validate_password[data-name="number"]').addClass('active');
					} else {
						$('.validate_password[data-name="number"]').removeClass('active');
					}

					//is lowercase
					if ($(this).val().match(lowerCase) != null) {
						$('.validate_password[data-name="lowercase"]').addClass('active');
					} else {
						$('.validate_password[data-name="lowercase"]').removeClass('active');
					}

					//is uppercase
					if ($(this).val().match(upperCase) != null) {
						$('.validate_password[data-name="uppercase"]').addClass('active');
					} else {
						$('.validate_password[data-name="uppercase"]').removeClass('active');
					}
				} else {
					$('.validate_password').removeClass('active');
				}
			});
		},

		validate_key: function () {
			//VALIDA A CHAVE DE ACESSO E CRIA SENHA
			$('#create_account').submit(function (event) {
				event.preventDefault();

				var senha_1 = $('#create_account input[name="senha_1"]').val();
				var senha_2 = $('#create_account input[name="senha_2"]').val();

				if (senha_1.length >= 8 && senha_2.length >= 8) {
					if (senha_1 === senha_2) {
						var email_login = $('#create_account input[name="confirm_email"]').val();
						var senha_login = $('#create_account input[name="senha_1"]').val();
						var autentica_login = $('#create_account input[name="confirm_chave"]').val();

						var settings = {
							"async": true,
							"crossDomain": true,
							"url": "/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=atacadoconscienciajeans",
							"method": "GET"
						}

						$.ajax(settings).done(function (response) {
							var entrada = {
								"async": true,
								"crossDomain": true,
								"url": "/api/vtexid/pub/authentication/classic/setpassword?authenticationToken=" + response.authenticationToken + "&newPassword=" + senha_login + "&login=" + email_login + "&accessKey=" + autentica_login,
								"type": "POST"
							}

							$.ajax(entrada).done(function (response) {
								if (response.authStatus == 'WrongCredentials') {
									swal("Oops", "Chave inválida!", "error");
								} else {
									//CADASTRO DE SENHA: OK
									//INPUTAR INFORMAÃ‡Ã•ES AO MASTER DATA
									$('#form_new_user button[type="submit"]').trigger('click');
									swal({
										title: 'Cadastro realizado com sucesso!',
										text: 'Agora você pode realizar compras normalmente.',
										icon: 'success',
										buttons: {
											cancel: false,
											confirm: true,
										},
										dangerMode: false,
									})
									.then((willDelete) => {
										if (willDelete) {
											window.location = window.origin;
										}
									});
								}
							}).fail(function (status, error) {
								let res = JSON.parse(status.responseText);
								console.log(res);
								if (res.authStatus == 'WrongCredentials') {
									swal("Oops", "Chave inválida!", "error");
								}
							});
						});
					} else {
						swal("Oops", "Senha inválida!", "error");
					}
				}
			});
		},

		isento: function () {
			$('input[name="ie_checkbox"]').on('click', function () {
				if ($('input[name="ie_checkbox"]:checked').length != 0) {
					$('#form_new_user input[name="ie"]').val('Isento');
					$('#form_new_user input[name="ie"]').attr('readonly', 'readonly');
				} else {
					$('#form_new_user input[name="ie"]').val('');
					$('#form_new_user input[name="ie"]').removeAttr('readonly');
				}
			});
		}
	}

	cadastro.send_token();
	cadastro.validate_password();
	cadastro.validate_key();
	cadastro.isento();

	var step = {
		edit: function () {
			$('#form_new_user .step_4 span.edit').on('click', function () {
				let step = $(this).data('step');

				$('#form_new_user .step').fadeOut(300, function () {
					setTimeout(function () {
						$('#form_new_user .step_' + step).fadeIn(300);
						$('.pagination, .timeline').attr('data-id', step);
					}, 300);
				});
			});
		},

		pagination: function () {
			$('.pagination span').on('click', function () {
				if ($(this).hasClass('back')) {
					var i = parseInt($('.pagination').attr('data-id')) - 1;

					if ($('.pagination').attr('data-id') === '1') {} else {
						$('.pagination, .timeline').attr('data-id', i);

						$('#form_new_user .step').fadeOut(300, function () {
							setTimeout(function () {
								$('#form_new_user .step_' + i).fadeIn(300);
							}, 300);
						});
					}

				} else if ($(this).hasClass('next')) {
					var i = parseInt($('.pagination').attr('data-id')) + 1;

					if (i < 5) {
						$('.pagination, .timeline').attr('data-id', i);

						$('#form_new_user .step').fadeOut(300, function () {
							setTimeout(function () {
								$('#form_new_user .step_' + i).fadeIn(300);
							}, 300);
						});
					}

					//CONFIMAÃ‡ÃƒO DE CADASTRO
					if (i === 4) {
						$('#form_new_user input').each(function (index, item) {
							let name = $(item).attr('name');
							let value = $(item).val();

							console.log(name);
							console.log(value);

							$('#form_new_user .step_4 ul article p[data-name="' + name + '"] span').text(value);
						});

						$('#form_new_user .step').fadeOut(300, function () {
							setTimeout(function () {
								$('#form_new_user .step_4').fadeIn(300);
							}, 300);
						});
					}
				}
			});

			//PRÃ“XIMO PASSO - CRIAR SENHA
			$('#form_new_user .pagination .register').on('click', function (e) {
				e.preventDefault();
				$('#alerta').addClass('active');
			});
		},

		send_info_user: function () {
			//CADASTRAR INFORMAÃ‡Ã•ES DO CLIENTE
			$('#form_new_user').submit(function (event) {
				event.preventDefault();

				$('#create_account input[type="submit"]').val('Aguarde...');

				//PEGA ID DO USUARIO
				$.ajax({
					type: 'GET',
					url: '/api/vtexid/pub/authenticated/user'
				}).done(function (user) {

					$('#form_new_user input[name="user-id"]').val(user.userId); // ID DO USUARIO

					var obj_CL = {
						"isCorporate": true,
						"firstName": $('#form_new_user input[name="nome"]').val(),
						"lastName": $('#form_new_user input[name="sobrenome"]').val(),
						"email": $('#form_new_user input[name="email"]').val(),
						"document": $('#form_new_user input[name="cpf"]').val(),
						"celular": $('#form_new_user input[name="celular"]').val(),
						"phone": $('#form_new_user input[name="phone"]').val(),
						"corporateDocument": $('#form_new_user input[name="cnpj"]').val(),
						"corporateName": $('#form_new_user input[name="razao-social"]').val(),
						"tradeName": $('#form_new_user input[name="nome-fantasia"]').val(),
						"dataFundacao": $('#form_new_user input[name="data-fundacao"]').val(),
						"matrizFilial": $('#form_new_user input[name="matriz-filial"]').val(),
						"capital": $('#form_new_user input[name="capital"]').val(),
						"codAtividadeEconomica": $('#form_new_user input[name="cod-atividade-economica"]').val(),
						"codAtividadeEconomicaDescricao": $('#form_new_user input[name="cod-atividade-economica-descricao"]').val(),
						"codNaturezaJuridica": $('#form_new_user input[name="cod-natureza-juridica"]').val(),
						"codNaturezaJuridicaDescricao": $('#form_new_user input[name="cod-natureza-juridica-descricao"]').val(),
						"situacaoRfb": $('#form_new_user input[name="situacao-rfb"]').val(),
						"dataSituacaoRfb": $('#form_new_user input[name="data-situacao-rfb"]').val(),
						"dataConsultaRfb": $('#form_new_user input[name="data-consulta-rfb"]').val(),
						"cnae": $('#form_new_user input[name="cnae"]').val(),
						"cnaeDescricao": $('#form_new_user input[name="cnae-descricao"]').val(),
						"dataAtualizacao": $('#form_new_user input[name="data-de-atualizacao"]').val(),
						"ibge": $('#form_new_user input[name="ibge"]').val(),
						"stateRegistration": $('#form_new_user input[name="ie"]').val()
					}

					var info_CL = JSON.stringify(obj_CL);

					var obj_AD = {
						"userId": $('#form_new_user input[name="email"]').val(),
						"postalCode": $('#form_new_user input[name="cep"]').val(),
						"street": $('#form_new_user input[name="logradouro"]').val(),
						"addressName": $('#form_new_user input[name="logradouro"]').val(),
						"number": $('#form_new_user input[name="numero"]').val(),
						"complement": $('#form_new_user input[name="complemento"]').val(),
						"neighborhood": $('#form_new_user input[name="bairro"]').val(),
						"city": $('#form_new_user input[name="cidade"]').val(),
						"state": $('#form_new_user input[name="estado"]').val()
					}

					var info_AD = JSON.stringify(obj_AD);

					insertMasterData("CL", 'atacadoconscienciajeans', info_CL, function (res) {
						console.log(res);
						console.log('CL enviado para MASTER DATA!');

						//AGUARDANDO ENTIDADE SER CRIADA
						setTimeout(function () {
							insertMasterData("AD", 'atacadoconscienciajeans', info_AD, function (res) {
								console.log(res);
								console.log('AD enviado para MASTER DATA!');

								//CADASTRO REALIZADO COM SUCESSO

								swal({
									title: 'Cadastro realizado com sucesso!',
									text: 'Agora vocÃª pode realizar compras normalmente.\n Enviamos no seu email a confirmaÃ§Ã£o.',
									icon: 'success',
									buttons: {
										cancel: false,
										confirm: true,
									},
									dangerMode: false,
								})
								.then((willDelete) => {
									if (willDelete) {
										window.location = window.origin;
									}
								});
							});
						}, 5000);
					});
				});
			});
		},

		consulta_cep: function () {
			$('#form_new_user input[name="cep"]').on('focusout', function () {
				var cep = $('#form_new_user input[name="cep"]').val();
				cep = cep.replace('-', '');

				$.ajax({
					url: 'https://viacep.com.br/ws/' + cep + '/json/',
					type: 'GET',
					success: function (res) {
						console.log(res);
						if (res.erro) {
							swal("Oops", "CEP inválido!", "error");
						} else {
							$('#form_new_user input[name="logradouro"]').val(res.logradouro);
							$('#form_new_user input[name="complement"]').val(res.complemento);
							$('#form_new_user input[name="bairro"]').val(res.bairro);
							$('#form_new_user input[name="cidade"]').val(res.localidade);
							$('#form_new_user input[name="estado"]').val(res.uf);
							$('#form_new_user input[name="ibge"]').val(res.ibge);
						}
					},
					error: function (res) {}
				});
			});
		},

		valida_cpf: function () {
			function TestaCPF(strCPF) {
				var Soma;
				var Resto;
				Soma = 0;
				if (
					strCPF == "00000000000" ||
					strCPF == "11111111111" ||
					strCPF == "22222222222" ||
					strCPF == "33333333333" ||
					strCPF == "44444444444" ||
					strCPF == "55555555555" ||
					strCPF == "66666666666" ||
					strCPF == "77777777777" ||
					strCPF == "88888888888" ||
					strCPF == "99999999999"
				) return false;

				for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
				Resto = (Soma * 10) % 11;

				if ((Resto == 10) || (Resto == 11)) Resto = 0;
				if (Resto != parseInt(strCPF.substring(9, 10))) return false;

				Soma = 0;
				for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
				Resto = (Soma * 10) % 11;

				if ((Resto == 10) || (Resto == 11)) Resto = 0;
				if (Resto != parseInt(strCPF.substring(10, 11))) return false;
				return true;
			}

			$('#form_new_user input[name="cpf"]').on('focusout', function () {
				var cpf = $('#form_new_user input[name="cpf"]').val();
				cpf = cpf.replace(/\./g, '');
				cpf = cpf.replace('-', '')

				if (TestaCPF(cpf) === true) {
					$('#form_new_user input[name="cpf"]').removeClass('alert');
				} else {
					$('#form_new_user input[name="cpf"]').addClass('alert');
					swal("Oops", "CPF inválido!", "error");
				}
			});
		}
	}

	step.send_info_user();
	step.valida_cpf();
	step.edit();
	step.pagination();
})();